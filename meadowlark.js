var express = require('express');

var app = express();



app.set('port',process.env.PORT || 3001);

// 设置 handlebars 视图引擎
var handlebars = require('express-handlebars').create({defaultLayout:'main'});
app.engine('handlebars',handlebars.engine);
app.set('view engine','handlebars');

// 给项目加上静态资源配置
app.use(express.static(__dirname + '/public'));

// 给首页和关于页面加上路由

app.get('/',function(req,res){
    res.render('home')
    // res.type('text/plain');
    // res.send('Meadowlark Travel');
})

const fortunes = [
    'Conquer your fears or they will conquer you.',
    'Rivers need springs',
    'Do not fear what you don\'t know',
    'You will have a pleasant surprise.',
    'Whenever possible, keep it simple.'
]

app.get('/about',function(req,res){
    let randomFortune  = fortunes[Math.floor(Math.random() * fortunes.length)]
    res.render('about',{ fortune:randomFortune}) // 通过对象给模板页面赋值
    // res.type('text/plain');
    // res.send('About Meadowlark Travel');
})

// 404 catch-all 处理器（中间件）
app.use(function(req,res,next){
    res.status(404);
    res.render('404');

})

// 500 catch-all 处理器 
app.use(function(err,req,res,next){
    console.error(err.stack);
    res.status(500);
    res.render('500');
})

// // 定制 404 页面

// app.use(function(req,res){
//     res.type('text/plain');
//     res.status(404);
//     res.send('404 - Not Found');
// })

// // 定制 500 页面

// app.use(function(err,req,res,next){
//     console.error(err.stack)
//     res.type('text/plain');
//     res.status(500);
//     res.send('500 - Server Error')
// })

app.listen(app.get('port'),function(){
    console.log('Express started on http://localhost:'+app.get('port')+';press Ctrl-C to terminate.')
})